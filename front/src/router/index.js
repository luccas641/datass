import Vue from 'vue'
import Router from 'vue-router'
import Form from '@/components/Form'
import Validation from '@/components/Validation'
import ConfirmReferee from '@/components/ConfirmReferee'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Form',
      component: Form
    },
    {
      path: '/validation',
      name: 'Validation',
      component: Validation
    },
    {
      path: '/validation/:token',
      name: 'ConfirmReferee',
      component: ConfirmReferee
    }
  ]
})
